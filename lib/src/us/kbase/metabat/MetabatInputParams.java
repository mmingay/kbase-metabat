
package us.kbase.metabat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * <p>Original spec-file type: MetabatInputParams</p>
 * <pre>
 * required params:
 * assembly_ref: Genome assembly object reference
 * workspace_name: the name of the workspace it gets saved to.
 * reads_list: list of reads object (PairedEndLibrary/SingleEndLibrary) upon which MetaBAT will be run
 * optional params:
 * min_contig_length: what size of contig to include in binning default is 2500bp
 * <maybe include unbinned option>
 * ref: https://bitbucket.org/berkeleylab/metabat
 * </pre>
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "assembly_ref",
    "binned_contig_name",
    "workspace_name",
    "reads_list",
    "min_contig_length"
})
public class MetabatInputParams {

    @JsonProperty("assembly_ref")
    private java.lang.String assemblyRef;
    @JsonProperty("binned_contig_name")
    private java.lang.String binnedContigName;
    @JsonProperty("workspace_name")
    private java.lang.String workspaceName;
    @JsonProperty("reads_list")
    private List<String> readsList;
    @JsonProperty("min_contig_length")
    private Long minContigLength;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    @JsonProperty("assembly_ref")
    public java.lang.String getAssemblyRef() {
        return assemblyRef;
    }

    @JsonProperty("assembly_ref")
    public void setAssemblyRef(java.lang.String assemblyRef) {
        this.assemblyRef = assemblyRef;
    }

    public MetabatInputParams withAssemblyRef(java.lang.String assemblyRef) {
        this.assemblyRef = assemblyRef;
        return this;
    }

    @JsonProperty("binned_contig_name")
    public java.lang.String getBinnedContigName() {
        return binnedContigName;
    }

    @JsonProperty("binned_contig_name")
    public void setBinnedContigName(java.lang.String binnedContigName) {
        this.binnedContigName = binnedContigName;
    }

    public MetabatInputParams withBinnedContigName(java.lang.String binnedContigName) {
        this.binnedContigName = binnedContigName;
        return this;
    }

    @JsonProperty("workspace_name")
    public java.lang.String getWorkspaceName() {
        return workspaceName;
    }

    @JsonProperty("workspace_name")
    public void setWorkspaceName(java.lang.String workspaceName) {
        this.workspaceName = workspaceName;
    }

    public MetabatInputParams withWorkspaceName(java.lang.String workspaceName) {
        this.workspaceName = workspaceName;
        return this;
    }

    @JsonProperty("reads_list")
    public List<String> getReadsList() {
        return readsList;
    }

    @JsonProperty("reads_list")
    public void setReadsList(List<String> readsList) {
        this.readsList = readsList;
    }

    public MetabatInputParams withReadsList(List<String> readsList) {
        this.readsList = readsList;
        return this;
    }

    @JsonProperty("min_contig_length")
    public Long getMinContigLength() {
        return minContigLength;
    }

    @JsonProperty("min_contig_length")
    public void setMinContigLength(Long minContigLength) {
        this.minContigLength = minContigLength;
    }

    public MetabatInputParams withMinContigLength(Long minContigLength) {
        this.minContigLength = minContigLength;
        return this;
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public java.lang.String toString() {
        return ((((((((((((("MetabatInputParams"+" [assemblyRef=")+ assemblyRef)+", binnedContigName=")+ binnedContigName)+", workspaceName=")+ workspaceName)+", readsList=")+ readsList)+", minContigLength=")+ minContigLength)+", additionalProperties=")+ additionalProperties)+"]");
    }

}
