
package us.kbase.metabat;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * <p>Original spec-file type: AlignmentInputParams</p>
 * <pre>
 * alignment: full path to reference (alignment) fasta.
 * fastq: full path to a single fastq file.
 * </pre>
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "assembly",
    "fastq"
})
public class AlignmentInputParams {

    @JsonProperty("assembly")
    private String assembly;
    @JsonProperty("fastq")
    private String fastq;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("assembly")
    public String getAssembly() {
        return assembly;
    }

    @JsonProperty("assembly")
    public void setAssembly(String assembly) {
        this.assembly = assembly;
    }

    public AlignmentInputParams withAssembly(String assembly) {
        this.assembly = assembly;
        return this;
    }

    @JsonProperty("fastq")
    public String getFastq() {
        return fastq;
    }

    @JsonProperty("fastq")
    public void setFastq(String fastq) {
        this.fastq = fastq;
    }

    public AlignmentInputParams withFastq(String fastq) {
        this.fastq = fastq;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ((((((("AlignmentInputParams"+" [assembly=")+ assembly)+", fastq=")+ fastq)+", additionalProperties=")+ additionalProperties)+"]");
    }

}
